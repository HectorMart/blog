=begin
class Article < ApplicationRecord
end
=end
=begin
class Article < ActiveRecord::Base
  validates :title, presence: true,
                    length: { minimum: 5 }
end
=end
=begin  19-07-2019  8.1
class Article < ActiveRecord::Base
  has_many :comments
  validates :title, presence: true,
                    length: { minimum: 5 }
end
=end
class Article < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates :title, presence: true,
                    length: { minimum: 5 }
end
