=begin
class Comment < ApplicationRecord
  belongs_to :article
end
=end

class Comment < ActiveRecord::Base
  belongs_to :article
end
